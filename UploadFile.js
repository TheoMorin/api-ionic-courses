const multer = require("multer");
const path = require('path');

class UploadFile {
    static fileStorage = multer.diskStorage({
        // Destination to store image     
        destination: 'public/content', 
        filename: (req, file, cb) => {
            cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname))
            // file.fieldname is name of the field (image)
            // path.extname get the uploaded file extension
        }
    });
    static fileUpload = multer({
        storage: UploadFile.fileStorage,
        limits: {
          fileSize: 100000000 // 1000000 Bytes = 1 MB
        },
        fileFilter(req, file, cb) {
            if (!file.originalname.match(/\.(png|jpg|jpeg|gif|mp4|avi|m4v|wmv|flv|webm|mov)$/i)) { 
                return cb(new Error('Please upload a Image or Video'))
            }
            cb(undefined, true)
        }
    });

    static PPStorage = multer.diskStorage({
        // Destination to store image     
        destination: 'public/profile', 
        filename: (req, file, cb) => {
            cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname))
            // file.fieldname is name of the field (image)
            // path.extname get the uploaded file extension
        }
    });
    static PPUpload = multer({
        storage: UploadFile.PPStorage,
        limits: {
          fileSize: 100000000 // 1000000 Bytes = 1 MB
        },
        fileFilter(req, file, cb) {
            console.log(file.originalname);
            if (!file.originalname.match(/\.(png|jpg|jpeg|gif)$/i)) { 
                return cb(new Error('Please upload a Image or Video'))
            }
            cb(undefined, true)
        }
    });
}
module.exports = UploadFile;