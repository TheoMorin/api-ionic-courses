const express = require('express');
const app = express();
const port = 3100;

const JWToken = require('./JWToken');
const UploadFile = require('./UploadFile.js');
const crypto = require('crypto');
const { Z_ASCII } = require('zlib');

const users = [];
const posts = [];

function getUser(userId){
    for(let user of users){
        if(user.userId == userId) {console.log(user); return user;}
        
    }
    return false;
}

function getMy(token) {
    let payload = JWToken.verify(token);
    return payload ? getUser(payload.userId) : false;
}

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', '*');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', '*');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/profiles/pictures', express.static(__dirname + "/public/profile"));
app.use('/posts/contents', express.static(__dirname + "/public/content"));

app.get("/", (req, res) => {
    res.status(200).send("Bienvenue sur l'API du cours Ionic-Angular. (API réalisé en NodeJS)");
})

app.post("/user/login", (req, res) => {
    
    let email = req.query.email;
    let password = req.query.password;
    console.log(email,password);
    if(email == undefined || password == undefined) res.status(400).send();
    var hash = crypto.createHash('sha256');
    password = hash.update(password).digest('hex');
    for(let user of users) {
        if(user.email == email && user.password == password) {
            notfound = false;
            let token = JWToken.load({iat:Date.now(), userId:user.userId, pseudonyme:user.pseudonyme});
            res.status(200).send({token:token});
        }
    }
    res.status(500).send("user not found");
});

app.post("/user/register", (req, res) => {
    console.log("query", req.query);
    let email = req.query.email;
    console.log("email", email);
    let pseudonyme = req.query.pseudonyme;
    let password = req.query.password;
    if(email == undefined || pseudonyme == undefined || password == undefined) res.status(400).send();
    var hash = crypto.createHash('sha256');
    password = hash.update(password).digest('hex');
    for(let user of users) {
        if(user.email == email) res.status(500).send("user already exist");
    }
    users.push({
        userId: users.length,
        pseudonyme:pseudonyme,
        email: email,
        password: password,
        profil: {
            picture:"http://placehold.it/300x300",
            followers: [],
            following: []
        }
    });
    res.status(204).send();
    console.log(users);
});

app.get("/user", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let user = getUser(req.query.userId);
    if(user) res.status(200).send({userId:user.userId, pseudonyme:user.pseudonyme,PPicture:user.profil.picture});
    else res.status(500).send("Unknown user.");
})

app.get("/user/following", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let userId = req.query.userId;
    let user = getUser(userId);
    let result = [];
    if(!user) res.status(500).send("Unknown user.");
    let following = user.profil.following;
    for(let u of users) {
        if(following.includes(u.userId)) {
            result.push({
                userId:u.userId,
                pseudonyme:u.pseudonyme,
                PPicture:u.profil.picture
            });
        }
    }
    res.status(200).send(result);
});

app.get("/user/following/count", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let userId = req.query.userId;
    let user = getUser(userId);
    if(!user) res.status(500).send("Unknown user.");
    res.status(200).send(user.profil.following.length);
});

app.get("/user/followers", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let userId = req.query.userId;
    let user = getUser(userId);
    let result = [];
    if(!user) res.status(500).send("Unknown user.");
    let followers = user.profil.followers;
    for(let u of users) {
        if(followers.includes(u.userId)) {
            result.push({
                userId:u.userId,
                pseudonyme:u.pseudonyme,
                PPicture:u.profil.picture
            });
        }
    }
    res.status(200).send(result);

});

app.get("/user/followers/count", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let userId = req.query.userId;
    let user = getUser(userId);
    if(!user) res.status(500).send("Unknown user.");
    res.status(200).send(user.profil.followers.length);
});

app.post("/user/follow", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let userId = req.query.userId;
    for(let user of users)
        if(user.userId == userId) {user.profil['following'].push(my.userId);res.status(204).send();}
    res.status(500).send();
});

app.get("/user/posts", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let results = [];
    let userId = req.query.userId;
    for(var i = posts.length - 1; i >= 0; i--)
        if(posts[i].userId == userId) {results.push(posts[i]);results[results.length-1].liked = posts[i].likes.includes(my.userId) ? true : false;}
    res.status(200).send(results);
});

app.post("/user/edit", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let email = req.query.email == undefined ? my.email : req.query.email;
    let pseudonyme = req.query.pseudonyme == undefined ? my.email : req.query.pseudonyme;
    let password = req.query.password;
    for(let user of users)
        if(user.userId == my.userId) {
            if(user.password == password) {
                user.pseudonyme = pseudonyme;
                user.password = password;
                res.status(204).send();
            }
        }

    res.status(500).send("Unknown user.");
});

app.post("/user/edit/password", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let old = req.query.old;
    let nouveau = req.query.new;

    let user = getUser(my.userId);
    if(user.password == old) {
        for(let user of users)
            if(user.userId == my.userId) user.password = nouveau;
        res.status(204).send();
    }
    res.status(403).send();
});

app.post("/user/edit/PPicture", UploadFile.PPUpload.single('attachment'), (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let attachment = req.file != undefined ? req.file.path : false;
    if(!attachment) res.status(500).send("Undefined attachment !");
    for(let user of users) {
        if(my.userId == user.userId) {
            attachment = attachment.replace('public/profile', '/profiles/pictures');
            user.profil.picture = attachment;
            res.status(200).send({url:attachment});
        }
    }
});

app.get("/search", (req, res) => {
    let query = req.query.query;
    let results = [];
    for(let user of users) {
        if(user.pseudonyme.startsWith(query)) results.push({userId:user.userId, pseudonyme:user.pseudonyme});
    }
    res.status(200).send(results);
})

app.get("/feed", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let results = [];

    if(posts.length > 0) {
        for(let i = posts.length - 1; i >= 0; i--)
            if(my.profil.following.includes(posts[i].userId)) results.push(posts[i]);
            else if(my.userId == posts[i].userId) results.push(posts[i]);
    }

    res.status(200).send(results);
});

app.get("/post", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    
    let postId = req.query.postId;
    for(let post of posts)
        if(post.postId == postId) res.status(200).send(post);
    res.status(500).send("Post doesn't exist.");
});

app.post("/posts", UploadFile.fileUpload.single('attachment'), (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let attachment = req.file != undefined ? req.file.path : "";
    attachment = attachment.replace('public/content', '/posts/contents');
    let content = req.query.content;
    posts.push({
        postId:posts.length == 0 ? 1 : posts[posts.length - 1].postId + 1,
        userId:my.userId,
        content:content,
        attachment:attachment,
        likes: [],
        comments: []
    });
    res.status(204).send();
});

app.delete("/posts", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let postId = req.query.postId;
    for(let i = posts.length - 1; i >= 0; i--) {
        let prev = posts[i - 1];
        posts[i-1] = posts[i];
        if(prev.postId == postId) break;
    }
    res.status(204).send();
});
app.post("/posts/like", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();

    let postId = req.query.postId;
    for(let i = posts.length - 1; i >= 0; i--) {
        if(posts[i].postId == postId) {
            posts[i]['likes'].push(my.userId);
            console.log(posts);
            res.status(204).send();
        }
    }
    res.status(500).send("Post doesn't exist.");
});
app.post("/posts/comment", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let postId = req.query.postId;
    let content = req.query.content;

    for(let i = posts.length - 1; i >= 0; i--) {
        if(posts[i].postId == postId) {
            posts[i]['comments'].push({userId:my.userId, content:content});
            res.status(204).send();
        }
    }
    res.status(500).send("Post doesn't exist.");
});

app.get("/suggests", (req, res) => {
    let my = getMy(req.header("token"));
    if(!my) res.status(401).send();
    let following = my.profil.following;
    let results = [];
    for(let user of users) {
        if(following.includes(user.userId)) {
            for(let i = 0; i < user.profil.following; i++) {
                if(!following.includes(user.profil.following[i])) {
                    let u = getUser(user.profil.following[i]);
                    results.push({
                        userId: u.userId,
                        pseudonyme: u.pseudonyme,
                        PPicture: u.profil.picture
                    });
                }
            }
        }
    }
    res.status(200).send(results);
});

app.listen(port, () => {
	console.log(`API IONIC launched at port ${port}`);
});
