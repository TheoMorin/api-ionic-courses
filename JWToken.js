const crypto = require('crypto');

class JWToken {
    static key = "1234"
    constructor() {}

    static load(payload) {
        let header = {"alg": "HS256","typ": "JWT"};
        header = Buffer.from(JSON.stringify(header)).toString('base64');
        payload = Buffer.from(JSON.stringify(payload)).toString('base64');
        let signature = header + '.' + payload;
        signature = crypto.createHmac('sha256', JWToken.key).update(signature).digest("base64");
        return header + '.' + payload + '.' + signature;
    }

    static verify(token) {
        try {
            token = token.split('.');
            let header = token[0];
            let payload = token[1];
            let signature = token[2];
            let verify = header + '.' + payload;
            verify = crypto.createHmac('sha256', JWToken.key).update(verify).digest("base64");
            return signature == verify ? JSON.parse(Buffer.from(payload, 'base64').toString()) : false;
        }catch(error) {return false;}
    }
}

module.exports = JWToken;